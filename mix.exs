defmodule ExEarlyRet.MixProject do
  use Mix.Project

  def project do
    [
      app: :ex_early_ret,
      name: "ex_early_ret",
      description: "An Elixir macro for limited early return (expands to nested if-else)",
      source_url: "https://codeberg.org/valpackett/ex_early_ret",
      version: "0.1.3",
      elixir: "~> 1.10",
      deps: deps(),
      package: package()
    ]
  end

  def application do
    []
  end

  defp deps do
    [
      {:ex_doc, "~> 0.28", only: [:dev, :test, :docs]}
    ]
  end

  defp package do
    [
      files: ["lib", "mix.exs", "README.md", "UNLICENSE"],
      maintainers: ["Val Packett"],
      licenses: ["Unlicense"],
      links: %{"Codeberg" => "https://codeberg.org/valpackett/ex_early_ret"}
    ]
  end
end
